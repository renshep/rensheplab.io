morse = { # . is dot, - is dash
	'a':'.-',
	'b':'-...',
	'c':'-.-.',
	'd':'-..',
	'e':'.',
	'f':'..-.',
	'g':'--.',
	'h':'....',
	'i':'..',
	'j':'.---',
	'k':'-.-',
	'l':'.-..',
	'm':'--',
	'n':'-.',
	'o':'---',
	'p':'.--.',
	'q':'--.-',
	'r':'.-.',
	's':'...',
	't':'-',
	'u':'..-',
	'v':'...-',
	'w':'.--',
	'x':'-..-',
	'y':'-.--',
	'z':'--..',
}

words = open('words.txt') # taken from https://github.com/dwyl/english-words/blob/master/words_alpha.txt
wpf = open('english-palindromes.txt','w')
mpf = open('morse-palindromes.txt','w')
bpf = open('english-and-morse-palindromes.txt','w')
results = {}
wordsp = {}
morsep = {}
bothp = {}

def w(f,s):
	f.write(s)
	f.write("\n")

for word in words:
	word = word[:-1]
	iwp = 0
	imp = 0
	# make the morse version
	mword = ''
	for let in word:
		mword = mword + morse[let]
	# is the enlish version a palindrome?
	if word == word[::-1]:
		w(wpf,word)
		iwp = 1
	# is the morse version a palindrome?
	if mword == mword[::-1]:
		w(mpf,mword + ' (' + word + ')')
		imp = 1
	res = {
		'morse':mword,
		'isWordPal':iwp,
		'isMorsePal':imp,
	}
	results[word] = res
	if iwp:
		wordsp[word] = res
	if imp:
		morsep[word] = res
	if iwp & imp:
		bothp[word] = res
		w(bpf,word + ' ' + mword)

